module gitlab.com/diamondburned/minimon

go 1.13

require (
	github.com/mattn/go-shellwords v1.0.9
	github.com/pkg/errors v0.9.1
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e
)
