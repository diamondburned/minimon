package main

import (
	"log"
	"os"
)

func main() {
	var cfg = getConfigPath()
	var mon = parseConfig(cfg)

	if err := mon.Start(); err != nil {
		log.Fatalln("Failed to run:", err)
	}

	mon.Wait()
}

func parseConfig(path string) *Monitor {
	f, err := os.Open(path)
	if err != nil {
		log.Fatalln("Failed to open cfg at " + path)
	}
	defer f.Close()

	m, err := ParseMonitor(f)
	if err != nil {
		log.Fatalln("Failed to parse cfg:", err)
	}

	return m
}

func getConfigPath() string {
	if len(os.Args) != 2 {
		println("Invalid usage: minimon /path/to/config")
		os.Exit(2)
	}

	return os.Args[1]
}
