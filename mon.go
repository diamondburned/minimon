package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"
	"sync"
	"time"

	"github.com/mattn/go-shellwords"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

type Monitor struct {
	Global    Process
	Processes []Process

	run  errgroup.Group
	done sync.WaitGroup
}

type Process struct {
	Command   string
	Arguments []string // * if Global

	LogWriter io.WriteCloser
	Refresh   bool // unused
	InitWait  time.Duration

	cmd *exec.Cmd
}

func (p *Process) Start(done *sync.WaitGroup, run *errgroup.Group) error {
	var args []string
	if len(p.Arguments) > 1 {
		args = p.Arguments[1:]
	}

	p.cmd = exec.Command(p.Arguments[0], args...)
	p.cmd.Stdout = p.LogWriter
	p.cmd.Stderr = p.LogWriter

	if err := p.cmd.Start(); err != nil {
		return errors.Wrap(err, "Failed to start")
	}

	run.Go(func() error {
		done.Add(1)
		var errCh = make(chan error)
		var mu sync.Mutex

		go func() {
			err := p.cmd.Wait()

			mu.Lock()
			defer mu.Unlock()

			if errCh != nil {
				errCh <- err
			}

			done.Done()
		}()

		select {
		case <-time.After(p.InitWait):
			mu.Lock()
			defer mu.Unlock()

			errCh = nil
			return nil

		case err := <-errCh:
			reason := errors.Wrap(err, p.Command+" failed")
			fmt.Fprintln(p.LogWriter, "\n"+reason.Error())

			return reason
		}
	})

	return nil
}

func (p *Process) Stop() error {
	if p.cmd.ProcessState.Exited() {
		return nil
	}

	if err := p.cmd.Process.Signal(os.Interrupt); err != nil {
		return errors.Wrap(err, "SIGINT failed")
	}
	return nil
}

func (p *Process) parseOpts(opts []string) error {
	for _, opt := range opts {
		parts := strings.SplitN(opt, "=", 2)
		if len(parts) < 2 {
			return errors.New("Invalid argument " + opt)
		}

		switch parts[0] {
		case "log":
			w, err := fileLog(parts[1])
			if err != nil {
				return errors.Wrap(err, "Error at "+opt)
			}

			p.LogWriter = w

		case "init":
			d, err := time.ParseDuration(parts[1])
			if err != nil {
				return errors.Wrap(err, "Invalid duration")
			}

			p.InitWait = d
		}
	}
	return nil
}

func ParseMonitor(cfg io.Reader) (*Monitor, error) {
	c := csv.NewReader(cfg)
	m := Monitor{}

	for {
		r, err := c.Read()
		if err != nil {
			if err == io.EOF {
				break
			}

			return nil, errors.Wrap(err, "Failed to parse")
		}

		p := Process{
			Command: r[0],
		}

		if len(r) > 1 {
			if err := p.parseOpts(r[1:]); err != nil {
				return nil, err
			}
		}

		if p.Command == "*" {
			m.Global = p
		} else {
			m.Processes = append(m.Processes, p)
		}
	}

	// Apply global to all
	for _, p := range m.Processes {
		if m.Global.LogWriter != nil {
			p.LogWriter = m.Global.LogWriter
		}
		if m.Global.Refresh {
			p.Refresh = true
		}
		if m.Global.InitWait > 0 {
			p.InitWait = m.Global.InitWait
		}
	}

	return &m, nil
}

func (m *Monitor) Start() error {
	for _, p := range m.Processes {
		if err := p.Start(&m.done, &m.run); err != nil {
			return err
		}
	}

	err := m.run.Wait()
	if err == nil {
		return nil
	}

	for _, p := range m.Processes {
		if err := p.Stop(); err != nil {
			fmt.Fprintln(p.LogWriter, "\n", "Failed to stop process:", err)
		}
	}

	return err
}

func (m *Monitor) Wait() {
	m.done.Wait()
}

func fileLog(path string) (io.WriteCloser, error) {
	f, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY, os.ModePerm)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to open "+path)
	}
	return f, nil
}

func parseShell(line string) ([]string, error) {
	p := shellwords.NewParser()
	p.ParseEnv = true
	p.ParseBacktick = true

	return p.Parse(line)
}
